.. manganu documentation master file, created by
   sphinx-quickstart on Sun Jun  7 15:52:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Manganu
=====================================
Read online all complete serial manga, manhwa or manhua for free.

This website was created to make easy for you to read online about their chapters or episode volumes. All manga, manhwa or manhua in manganu website is already translated in English. 

You are able to read online manga, manhwa or manhua in multiple devices, desktop or mobile, the requirement is you have to open your chrome browser, then click link at below here.

Visit: `https://manganu.com <https://manganu.com>`_

Please follow our social network

Twitter: `https://twitter.com/manganu_dev <https://twitter.com/manganu_dev>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

